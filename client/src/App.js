

/*
function App() {

  
  

  return (
    <div className="App">
      <Header></Header>

    </div>
  );
}
*/

import React, { Component } from 'react'
import logo from './logo.svg';
import './App.css';

import Test from './Component/Test'
import HeaderSearch from './Component/HeaderSearch'
import  ShowGallerie from './Component/ShowGallerie'

import axios from 'axios';
//http://api.tvmaze.com/search/shows?q=friends
const urlSearchShow = '​http://api.tvmaze.com/search/shows?q=';

export class App extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       searchStr : '',
       listShow : [],
    }
  }

  handleSearch = (event) => {
    console.log(this.state.searchStr)
    this.setState({searchStr : event.target.value})
  }

  handleSubmit = () => {
    console.log('axios')
    if (this.state.searchStr !== '')
    {
      let finalUrl = urlSearchShow + this.state.searchStr;

      console.log(finalUrl)

      axios.get("http://api.tvmaze.com/search/shows?q=" + this.state.searchStr)
      .then((elem) => {
        let listShow = elem.data;
        console.log(listShow);
        // store show
        this.setState({searchStr : '', listShow : listShow})
      })
      .catch((err) => {
        console.log(err)
      })
    }
  }

  render() {
    return (
      <>
        <HeaderSearch handleSearch = {this.handleSearch} handleSubmit = {this.handleSubmit}></HeaderSearch>
        <ShowGallerie listShow={this.state.listShow}></ShowGallerie>        
      </>
    )
  }
}



export default App;
