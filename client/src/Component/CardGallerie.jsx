import React from 'react'

import { Card, CardMedia, CardContent, Typography, Paper } from '@material-ui/core';

import Chip from '@material-ui/core/Chip';

import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    cardMedia: {
        margin: "auto",
        marginBottom : '20px',
        marginTop : '20px'

    }
}))

export default function CardGallerie(props) {
    const classes = useStyles();
    let imageUrl = 'https://image.shutterstock.com/image-vector/none-vector-hand-drawn-illustration-260nw-1504674191.jpg'; // if image not found

    if (props.image !== null) {
        if (props.image.hasOwnProperty('medium'))
            imageUrl = props.image.medium;
        else if (props.image.resolutions.hasOwnProperty('original'))
            imageUrl = props.image.original;
    }

    console.log(props)
    return (
        <div>
            <Paper elevation={3}>
            <Card>
                <CardMedia
                    image={imageUrl}
                    style={{ width: "300px", height: "300px" }}
                    className={classes.cardMedia} />
                <CardContent>
                    <Typography align="center" variant="h6">{props.name} gutterBottom</Typography>
                    <Chip
                        label={props.score}
                        color="secondary"
                        size="small"
                    />
                    <Chip
                        label={props.genres.join('-')}
                        color="primary"
                        size="small"
                    />
                    <Typography paragraph align="justify" variant="body1">{props.summary}</Typography>

                </CardContent>
            </Card>
            </Paper>
        </div>
    )
}
