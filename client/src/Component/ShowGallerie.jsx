import React from 'react'
import CardGallerie from './CardGallerie'

import {Grid, Paper} from '@material-ui/core'

export default function ShowGallerie(props) {
    console.log(props)
 
    return (
        <>
            <Grid container item spacing={3}   justify="center" alignItems="stretch" alignContent="center">
            {
                props.listShow.length != 0 &&
                props.listShow.map((elem, index) =>
                    <>
                    <Grid item xs={12} xl={4} lg={4} md={4} sm={5} >
                    <CardGallerie name={elem.show.name}
                                  genres = {elem.show.genres}
                                  image={elem.show.image}
                                  summary={elem.show.summary.substring(0, 300)}
                                  score={elem.score}>
                    </CardGallerie>
                    </Grid>
                    </>
                )
            }
            </Grid>
        </>
    )
}
