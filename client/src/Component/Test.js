import React, { Component } from 'react'
import axios from 'axios';

const req = 'http://localhost:3000/minimal';

export default class Test extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }

    apiGet = () => {
        axios.get(req)
        .then((elem) => {
            console.log(elem);
        })
        .catch(err => {
            console.log(err);
        })
    }

    apiPost = () => {
        axios.post(req, {id : 42, miaou : 'coucou'})
        .then((elem) => {
            console.log('perform post');
        })
        .catch((err) => {
            console.log('error post')
        })
    }

    apiPut = () => {
        axios.put(req, {id : 42, miaou : 'john'})
        .then((elem) => {
            console.log('perform put')
        })
        .catch((err) => {
            console.log('error put')
        })
    }
    

    apiDelete = () => {
        axios.delete(req + '/42')
        .then((elem) => {
            console.log('perform delete')
        })
        .catch((err) => {
            console.log('error delete')
        })
    }

    render() {
        return (
            <div>
                <button onClick={this.apiGet}>get request</button>
                <button onClick={this.apiPost}>post request</button>
                <button onClick={this.apiPut}>put request</button>
                <button onClick={this.apiDelete}>delete request</button>
            </div>
        )
    }
}
 